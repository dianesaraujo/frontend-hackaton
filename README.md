link: https://tourmaline-griffin-ac0998.netlify.app/


Descrição
Projeto de sistema deu feedbacks que permite cadastrar um funcionário para ser avaliado, cadastrar avaliadores, criar uma nova avaliação e acompanhar o 
Desenvolvimento do funcionário. 
 



Requisitos: 
    •Controle de perfis;
    •Envio de e-mail;
    •Autenticação;
    •Desenvolvimento de formulários;
    •Usabilidade e experiência do usuário.



Funcionalidades: 

Administração geral
    Cadastrar leaguers;
    Ver todos os leaguers;
    Editar todos os leaguers;
    Abrir avaliação para todos os leaguers;
    Gerenciar acessos.

Mentor
    Cadastrar leaguers;
    Ver todos os leaguers;
    Editar todos os leaguers;
    Abrir avaliações para seus leaguers;

Gestor
    Ver apenas os leaguers pelos quais é responsável;
    Abrir avaliações para seus leaguers.


  Desenvolvedores
    Diane Silva de Araujo
    Jhordan Borges Almeida de Lacerda
    Vinicius Cicone Barbosa
    Ronald Ferreira Santiago



    Funcionalidades

    Header
    Header da plataforma presente em todas as páginas, que conta com o nome do site, um botão para ir para a página Home.
    ( A FAZER)
    Login
    Página inicial com o logo da da empresa, e o login  (usuário e senha )
    Home's
    Após o login cada usuário vai ser direcionado para sua home, existem 3: ADMINISTRADOR, MENTOR e GESTOR